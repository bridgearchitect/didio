# global libraries
import numpy as np

# define default constants
aInit = 6.0
bInit = 6.0
kInit = 20
cInit = 6.0
nInit = 4000
rCoef = 4.0
criticalArea = 1.0
radius = 2.0
# define mathematical constants
e = 2.718281828
# determine infinity constants
inf = 100000000.0
minInf = -100000000.0
# define index constants
noIndex = -1

# define structure to describe cell
class Cell:
    # define constructor
    def __init__(self, xInit, yInit, h, points):
        # initialization of properties
        self.xInit = xInit
        self.yInit = yInit
        self.h = h
        self.points = points

# function to receive value of mathematical expression for differential operator
def getExpressionValue(x, y):
    return e**(-x * x - y * y)

# function to receive the second order derivative along x-axis of mathematical expression
def getExpressionXXDer(x, y):
    return -2.0 * e**(-x**2 - y**2) + 4.0 * e**(-x**2 - y**2) * x**2

# function to receive the second order derivative along y-axis of mathematical expression
def getExpressionYYDer(x, y):
    return -2.0 * e**(-x**2 - y**2) + 4.0 * e**(-x**2 - y**2) * y**2

# function to receive value of the second derivative along x-axis for kernel function
def getKernalXXDer(x, y, eps):
    # calculate and return derivative
    derFunc = (5.49380 * x**6) / (eps**10 * (1.0 + x**4 / eps**4 + y**4 / eps**4)**3) - \
              (2.06018 * x**2) / (eps**6 * (1.0 + x**4 / eps**4 + y**4 / eps**4)**2)
    return derFunc

# function to receive value of the second derivative along y-axis for kernel function
def getKernalYYDer(x, y, eps):
    # calculate and return derivative
    derFunc = (5.49380 * y**6) / (eps**10 * (1.0 + x**4 / eps**4 + y**4 / eps**4)**3) - \
              (2.06018 * y**2) / (eps**6 * (1.0 + x**4 / eps**4 + y**4 / eps**4)**2)
    return derFunc

# function to calculate L2 distance between two points
def calculateL2DistanceTwoPoints(x1, y1, x2, y2):
    # caclulate Euclidean distance between two points
    return ((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1))**(0.5)

# function to calculate L0 distance between two points
def calculateL0DistanceTwoPoints(x1, y1, x2, y2):
    # caclulate L0 distance between two points
    return max([abs(x1 - x2), abs(y1 - y2)])

# function to calculate absolute value of cross product
def calculateAbsCrossProduct(ax, bx, ay, by):
    return abs(ax * by - ay * bx)

# function to create array of cells
def createArrayCells(a, k):

    # create array of cells
    cells = []
    for i in range(k):
        col = []
        for j in range(k):
            # create new cell
            cell = Cell(0.0, 0.0, a / k, np.zeros((0, 3), dtype=np.float64))
            # add cell to array
            col.append(cell)
        cells.append(col)

    # return array of cells
    return cells

# function to reduce information about points
def reduceInformationPoints(points):

    # initialization
    n = len(points)
    pointsRed = np.zeros((n,2))

    # go through all points
    for i in range(n):
        # copy coordinates
        pointsRed[i][0] = points[i][0]
        pointsRed[i][1] = points[i][1]

    # return reduced array
    return pointsRed

# function to find maximum and minimum coordinates for array of points
def findMaxMinCoordinates(points):

    # initialization
    xmin = inf
    ymin = inf
    xmax = minInf
    ymax = minInf
    n = len(points)

    # go through all points
    for i in range(n):
        # receive coordinates of point
        xcur = points[i][0]
        ycur = points[i][1]
        # find maximal and minimal coordinates
        if xcur > xmax:
            xmax = xcur
        if xcur < xmin:
            xmin = xcur
        if ycur > ymax:
            ymax = ycur
        if ycur < ymin:
            ymin = ycur

    # return maximal and minimal coordinates
    return xmax, xmin, ymax, ymin

# function to calculate 10 monomial
def calculateP10(x, y):
    return x

# function to calculate 01 monomial
def calculateP01(x, y):
    return y

# function to calculate 11 monomial
def calculateP11(x, y):
    return x * y

# function to calculate 20 monomial
def calculateP20(x, y):
    return x * x

# function to calculate 02 monomial
def calculateP02(x, y):
    return y * y

# function to calculate XX-derivative for 10 monomial
def calculateP10XX(x, y):
    return 0.0

# function to calculate XX-derivative for 01 monomial
def calculateP01XX(x, y):
    return 0.0

# function to calculate XX-derivative for 11 monomial
def calculateP11XX(x, y):
    return 0.0

# function to calculate XX-derivative for 20 monomial
def calculateP20XX(x, y):
    return 2.0

# function to calculate XX-derivative for 02 monomial
def calculateP02XX(x, y):
    return 0.0

# function to calculate YY-derivative for 10 monomial
def calculateP10YY(x, y):
    return 0.0

# function to calculate YY-derivative for 01 monomial
def calculateP01YY(x, y):
    return 0.0

# function to calculate YY-derivative for 11 monomial
def calculateP11YY(x, y):
    return 0.0

# function to calculate YY-derivative for 20 monomial
def calculateP20YY(x, y):
    return 0.0

# function to calculate YY-derivative for 02 monomial
def calculateP02YY(x, y):
    return 2.0
