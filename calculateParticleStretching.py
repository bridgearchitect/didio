#!/usr/bin/python3

# global libraries
import numpy as np
import sys, getopt
import math
from scipy.spatial import Voronoi, voronoi_plot_2d

# local libraries
from general import *

# define number constants
alphaMin = 1
numClosestPoints = 5
infinity = 1000000.0
noIndex = -1

# function to read options from terminal
def readOptions(argv):

    # initialize using default constants
    a = aInit
    b = bInit
    c = cInit
    k = kInit

    # read options from terminal
    try:
        opts, args = getopt.getopt(argv,"ha:b:c:k:",["anum=","bnum=","cnum=","knum="])
    except getopt.GetoptError:
      print('calculate.py -a <number> -b <number> -c <number> -k <number>')
      sys.exit(2)

    # handle options
    for opt, arg in opts:
        if opt == '-h':
            print('calculate.py -a <number> -b <number> -c <number> -k <number>')
            sys.exit()
        elif opt in ("-a", "--anum"):
            a = float(arg)
        elif opt in ("-b", "--bnum"):
            b = float(arg)
        elif opt in ("-c", "--cnum"):
            c = float(arg)
        elif opt in ("-k", "--knum"):
            k = int(arg)

    # return received information
    return a, b, c, k

# function to read information from STDIN thread
def readInformation(a, b, k):

    # create numpy array for points
    points = np.zeros((0, 3), dtype=np.float64)
    averDist = inf
    # create array of cells
    cells = createArrayCells(a, k)

    # launch reading
    while True:
        # read new line
        line = sys.stdin.readline()
        if not line:
            break
        # split into substrings
        substrings = line.split()
        # consider two situations
        if len(substrings) == 7:
            # convert strings to numbers
            xcur = float(substrings[0])
            ycur = float(substrings[1])
            val = float(substrings[2])
            xInit = float(substrings[3])
            yInit = float(substrings[4])
            xIndex = int(substrings[5])
            yIndex = int(substrings[6])
            # save information in array of points
            points = np.append(points, [[xcur, ycur, val]], axis=0)
            # save information in array of cells
            cells[xIndex][yIndex].xInit = xInit
            cells[xIndex][yIndex].yInit = yInit
            cells[xIndex][yIndex].points = np.append(cells[xIndex][yIndex].points, [[xcur, ycur, val]], axis=0)
        elif len(substrings) == 1:
            # save information in variable of average distance
            averDist = float(substrings[0])

    # return received information
    return points, cells, averDist

# function to insert point inside array
def insertPointInClosestArray(points, closestPoints, i, j):

    # default situation
    if i == j:
        return
    # calculate distance between two points
    xp = points[i][0]
    yp = points[i][1]
    xc = points[j][0]
    yc = points[j][1]
    dist = calculateL2DistanceTwoPoints(xp, yp, xc, yc)

    # go through all closest points
    for k in range(len(closestPoints[i])):
        # find index
        index = closestPoints[i][k]
        # calculate distance for the given closest point
        distClosest = infinity
        if index != noIndex:
            xc = points[index][0]
            yc = points[index][1]
            distClosest = calculateL2DistanceTwoPoints(xp, yp, xc, yc)
        # compare two distances
        if dist < distClosest:
            # make insertion
            t = k
            swap1 = closestPoints[i][t]
            while t < len(closestPoints[i]):
                if t == k:
                    closestPoints[i][t] = j
                else:
                    swap2 = swap1
                    swap1 = closestPoints[i][t]
                    closestPoints[i][t] = swap2
                t += 1
            # exit loop
            break

# function to find closest points for every points in array
def findClosestPoints(points, numClosestPoints):

    # find number of points
    numPoints = len(points)
    # create numpy array for the closest points
    closestPoints = np.zeros((numPoints, numClosestPoints), dtype=int)
    # initialize array
    for i in range(len(closestPoints)):
        for j in range(len(closestPoints[i])):
            closestPoints[i][j] = noIndex

    # go through all points
    for i in range(numPoints):
        for j in range(numPoints):
            insertPointInClosestArray(points, closestPoints, i, j)

    # return array of closestPoints
    return closestPoints

# function to calculate diagonal matrices
def calculateDiagonalMatrices(points, closestPoints, eps):

    # find number of points
    numPoints = len(points)
    # create object to save array of matrices
    diagMatrices = np.zeros((numPoints, numClosestPoints, numClosestPoints), dtype=float)

    # go through all points
    for i in range(numPoints):
        for j in range(numClosestPoints):
            # find coordinates of point
            xp = points[i][0]
            yp = points[i][1]
            xc = points[j][0]
            yc = points[j][1]
            # calculate diagonal elements
            diagMatrices[i][j][j] = e**(-1.0 * ((xp - xc)**2 + (yp - yc)**2) / (2.0 * eps * eps))

    # return array of diagonal matrices
    return diagMatrices

# function to calculate Vandermonde matrices
def calculateVandermondeMatrices(points, closestPoints, eps, monFunctions):

    # find number of points
    numPoints = len(points)
    # create object to save array of matrices
    vandMatrices = np.zeros((numPoints, numClosestPoints, numClosestPoints), dtype=float)

    # go through all points
    for i in range(numPoints):
        for j in range(numClosestPoints):
            for k in range(numClosestPoints):
                # find coordinates of point
                xp = points[j][0]
                yp = points[j][1]
                xc = points[k][0]
                yc = points[k][1]
                # calculate element of matrix
                vandMatrices[i][j][k] = monFunctions[k]((xp - xc) / eps, (yp - yc) / eps)

    # return array of Vandermonde matrices
    return vandMatrices

# function to calculate main matrices for particle stretching method
def calculateMainMatrices(diagMatrices, vandMatrices):

    # find number of points
    numPoints = len(points)
    # create object to save array of intermediate and matrices
    interMatrices = np.zeros((numPoints, numClosestPoints, numClosestPoints), dtype=float)
    mainMatrices = np.zeros((numPoints, numClosestPoints, numClosestPoints), dtype=float)

    # go through all points
    for i in range(numPoints):
        # calculate intermediate matrices
        interMatrices[i] = np.dot(vandMatrices[i].transpose(), diagMatrices[i])
        # calculate main matrices
        mainMatrices[i] = np.dot(interMatrices[i].transpose(), interMatrices[i])

    # return array of main matrices
    return mainMatrices

# function to calculate main vectors for particle stretching method
def calculateMainVector(points, monDerFunctions):

    # find number of points
    numPoints = len(points)
    # create object to save array of main vectors
    mainVectors = np.zeros((numPoints, numClosestPoints), dtype=float)

    # go through all points
    for i in range(numPoints):
        for j in range(numClosestPoints):
            # calculate value for main vectors
            mainVectors[i][j] = monDerFunctions[j](0.0, 0.0)

    # return array of main vectors
    return mainVectors

# function to solve systems of linear equations
def solveSystemsLinearEquaions(mainMatrices, mainVectors):

    # find number of points
    numPoints = len(points)
    # create object to save array of coefficient vectors
    coefVectors = np.zeros((numPoints, numClosestPoints), dtype=float)
    # go through all points
    for i in range(numPoints):
        coefVectors[i] = np.linalg.solve(mainMatrices[i], mainVectors[i])
    # return array of coefficient vectors
    return coefVectors

# function to calculate derivatives for every point
def calculateDerivative(coefVectors, monFunctions, points, closestPoints, eps):

    # find number of points
    numPoints = len(points)
    # create array to save derivatives
    derValueArray = np.zeros(numPoints, dtype=float)

    # go through all points
    for i in range(numPoints):
        # find coordinates and value of point
        xp = points[i][0]
        yp = points[i][1]
        valp = points[i][2]
        # calculate derivative
        derValue = 0.0
        for j in range(len(closestPoints[i])):
            # find coordinates of current point
            index = closestPoints[i][j]
            xc = points[index][0]
            yc = points[index][1]
            valc = points[index][2]
            # add new term to derivative value
            derValue += (1.0 / eps**2) * (valc - valp) * monFunctions[j]((xp - xc) / eps, (yp - yc) / eps) * \
            coefVectors[i][j] * e**(-1.0 * ((xp - xc)**2 + (yp - yc)**2) / eps**2)
        # save value of derivative
        derValueArray[i] = derValue

    # return array of derivatives
    return derValueArray


# function to print new calculated information
def printInformation(points, derValueArrayXX, derValueArrayYY):

    # go through all points
    n = len(points)
    for i in range(n):
        # print information about new point and its derivatives
        sys.stdout.write(str(points[i][0]) + " " + str(points[i][1]) + \
                         " " + str(points[i][2]) + " " + str(derValueArrayXX[i]) + \
                         " " + str(derValueArrayYY[i]) + "\n")

# read options from terminal
a, b, c, k = readOptions(sys.argv[1:])
# define array of monomial functions
monFunctions = [calculateP10, calculateP01, calculateP11, calculateP20, calculateP02]
# define arrays of derivative monomial functions
monDerFunctionsXX = [calculateP10XX, calculateP01XX, calculateP11XX, calculateP20XX, calculateP02XX]
monDerFunctionsYY = [calculateP10YY, calculateP01YY, calculateP11YY, calculateP20YY, calculateP02YY]
# read information from STDIN thread
points, cells, averDist = readInformation(a, b, k)
# calculate epsilon for weight function
eps = c * averDist * 8.0
# find closest points for every point
closestPoints = findClosestPoints(points, numClosestPoints)
# calculate array of diagonal matrices
diagMatrices = calculateDiagonalMatrices(points, closestPoints, eps)
# calculate array of Vandermonde matrices
vandMatrices = calculateVandermondeMatrices(points, closestPoints, eps, monFunctions)
# calculate array of main matrices
mainMatrices = calculateMainMatrices(diagMatrices, vandMatrices)
# calculate arrays of main vectors
mainVectorsXX = calculateMainVector(points, monDerFunctionsXX)
mainVectorsYY = calculateMainVector(points, monDerFunctionsYY)
# solve systems of linear equations
coefVectorsXX = solveSystemsLinearEquaions(mainMatrices, mainVectorsXX)
coefVectorsYY = solveSystemsLinearEquaions(mainMatrices, mainVectorsYY)
# find numerical values for derivatives
derValueArrayXX = calculateDerivative(coefVectorsXX, monFunctions, points, closestPoints, eps)
derValueArrayYY = calculateDerivative(coefVectorsYY, monFunctions, points, closestPoints, eps)
# print information about found numerical derivatives
printInformation(points, derValueArrayXX, derValueArrayYY)
