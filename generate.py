#!/usr/bin/python3

# global libraries
import numpy as np
import sys, getopt
import random
import math

# local libraries
from general import *

# function to read options from terminal
def readOptions(argv):

    # initialize using default constants
    a = aInit
    b = bInit
    n = nInit
    k = kInit

    # read options from terminal
    try:
        opts, args = getopt.getopt(argv,"ha:b:k:n:",["anum=","bnum=","knum=","number="])
    except getopt.GetoptError:
      print('generate.py -a <number> -b <number> -k <number> -n <number>')
      sys.exit(2)

    # handle options
    for opt, arg in opts:
        if opt == '-h':
            print('generate.py -a <number> -b <number> -k <number> -n <number>')
            sys.exit()
        elif opt in ("-a", "--anum"):
            a = float(arg)
        elif opt in ("-b", "--bnum"):
            b = float(arg)
        elif opt in ("-n", "--number"):
            n = int(arg)
        elif opt in ("-k", "--knum"):
            k = int(arg)

    # return received information
    return a, b, n, k

# function to generate random points
def generatePoints(a, b, n):

    # create numpy array
    points = np.zeros((n, 3), dtype=np.float64)
    # go through points
    for i in range(n):
        # initialize coordinates of random points
        points[i][0] = a * random.random() - a / 2.0
        points[i][1] = b * random.random() - b / 2.0
        points[i][2] = getExpressionValue(points[i][0], points[i][1])
    # return generated array of points
    return points

# function to generate cells for random points
def generateCells(points, a, b, k):

    # find number of points
    n = len(points)
    # find size of array
    h = a / k

    # create two dimensional array of lists
    cells = createArrayCells(a, k)
    for i in range(k):
        for j in range(k):
            # change initial coordinates of cell
            cells[i][j].xInit = -a / 2.0 + a * i / k
            cells[i][j].yInit = -b / 2.0 + b * j / k

    # go through all points
    for index in range(n):
        # receive coordinates of current point
        xcur = points[index][0]
        ycur = points[index][1]
        # define indexes in cell list
        i = int(math.modf(((xcur + a / 2.0) / h))[1])
        j = int(math.modf(((ycur + b / 2.0) / h))[1])
        # add new point to the corresponding cell
        cells[i][j].points = np.append(cells[i][j].points, [[points[index][0], points[index][1], points[index][2]]], axis=0)

    # return created array of cells
    return cells

# function to calculate avarage minimal distance among all points
def calculateAvarageMinDist(points):

    # initialize average distance
    averDist = 0.0
    # find number of points
    n = len(points)

    # go through all points
    for i in range(n):
        # find coordinates of point
        xcur = points[i][0]
        ycur = points[i][1]
        # initialize initial minimal distance
        minDist = inf
        # go through all points
        for j in range(n):
            if i != j:
                # caclulate distance between two points
                xcan = points[j][0]
                ycan = points[j][1]
                dist = calculateL2DistanceTwoPoints(xcur, ycur, xcan, ycan)
                # find minimal L2 distance for specified point
                if dist < minDist:
                    minDist = dist
        # calculate average distance
        averDist += minDist

    # normalize and return avarage distance
    averDist = averDist / n
    return averDist

# function to print information in STDOUT thread
def printInformation(cells, averDist):

    # go through all cells
    nRow = len(cells)
    for i in range(nRow):
        nCol = len(cells[i])
        for j in range(nCol):
            # go through all points of the corresponding cell
            nPoints = len(cells[i][j].points)
            for k in range(nPoints):
                sys.stdout.write(str(cells[i][j].points[k][0]) + " " + str(cells[i][j].points[k][1]) + " " +
                             str(cells[i][j].points[k][2]) + " " + str(cells[i][j].xInit) + " " + str(cells[i][j].yInit) + " " + \
                             str(i) + " " + str(j) + "\n")
    # print information about average distance
    sys.stdout.write(str(averDist) + "\n")

# return numbers from terminal
a, b, n, k = readOptions(sys.argv[1:])
# generate array of points
points = generatePoints(a, b, n)
# generate array of cells
cells = generateCells(points, a, b, k)
# calculate average minimal distance
averDist = calculateAvarageMinDist(points)
# print generated information
printInformation(cells, averDist)
