#!/usr/bin/python3

# global libraries
import numpy as np
import sys, getopt
import math

# local libraries
from general import *

# function to read information from STDIN thread
def readInformation():

    # create numpy array
    points = np.zeros((0, 3), dtype=np.float64)
    laplArray = np.zeros((0, 2), dtype=np.float64)

    # launch reading
    while True:
        # read new line
        line = sys.stdin.readline()
        if not line:
            break
        # split into substrings
        substrings = line.split()
        # consider two situations
        if len(substrings) == 5:
            # convert strings to numbers
            xcur = float(substrings[0])
            ycur = float(substrings[1])
            val = float(substrings[2])
            derXX = float(substrings[3])
            derYY = float(substrings[4])
            # save information in array of points and derivatives
            points = np.append(points, [[xcur, ycur, val]], axis=0)
            laplArray = np.append(laplArray, [[derXX, derYY]], axis=0)

    # return received information
    return points, laplArray

# function to compare numerical results with theory
def compareResultsWithTheory(points, laplArray):

    # find number of points
    n = len(points)
    # initialization
    error = 0.0
    numDOF = 0

    # go through all points
    for i in range(n):
        # receive all possible parameters of current point
        xcur = points[i][0]
        ycur = points[i][1]
        val = points[i][2]
        derXX = laplArray[i][0]
        derYY = laplArray[i][1]
        # calculate theoretical detivatives
        derXXTheory = getExpressionXXDer(xcur, ycur)
        derYYTheory = getExpressionYYDer(xcur, ycur)
        # calculate unnormalized error
        error += (derXXTheory - derXX)**2 + (derYYTheory - derYY)**2
        # clarify number of DOFs
        numDOF += 2

    # normalize calculated error
    error = error / numDOF
    error = math.sqrt(error)

    # return calculated error
    return error

# function to print information about comparison
def printInformation(error):

    # print about the found error
    sys.stdout.write("L2 error: " + str(error) + "\n")

# read information from STDIN thread
points, laplArray = readInformation()
# compare numerical and theoretical results
error = compareResultsWithTheory(points, laplArray)
# print information about comparison
printInformation(error)
